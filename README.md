Scripts to generate the baseline system for the WebNLG Challenge 2017.

See more details [here](http://talc1.loria.fr/webnlg/stories/challenge.html).
